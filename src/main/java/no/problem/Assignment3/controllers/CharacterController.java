package no.problem.Assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * CharacterController that is used as an itermediary between the user and the
 * Services. The CharacterController uses the method calls from the CharacterService
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/character") // da base
public class CharacterController {
    @Autowired
    CharacterService characterService;

    @Operation(summary = "Gets all characters in the database")
    @GetMapping("/all")
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        return characterService.getAllCharacters();
    }

    @Operation(summary = "Gets a single character based on its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Character has been found"),
            @ApiResponse(responseCode = "400", description = "Character could not be found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getSpecificMovieCharacter(@PathVariable Long id){
        return characterService.getSpecificMovieCharacter(id);
    }

    @Operation(summary = "Adds a single character to the database")
    @ApiResponse(responseCode = "201", description = "Character has been added")
    @PostMapping("/add")
    public ResponseEntity<MovieCharacter> addMovieCharacter(@RequestBody MovieCharacter mc){
        return characterService.addMovieCharacter(mc);
    }

    @Operation(summary = "Update a single character using its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Character has been updated"),
            @ApiResponse(responseCode = "400", description = "Character for update could not be found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateMovieCharacter(@RequestBody MovieCharacter mc, @PathVariable Long id){
        return characterService.updateMovieCharacter(mc,id);
    }

    @Operation(summary = "Delete a single character using its Id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Set<MovieCharacter>> deleteMovieCharacter(@PathVariable Long id) {
        return characterService.deleteMovieCharacter(id);
    }

    @Operation(summary = "Gets all characters that played in a specific movie using the Movies Id")
    @GetMapping("/movie/{id}")
    public ResponseEntity<Set<MovieCharacter>> findAllCharactersInMovie(@PathVariable Long id) {
        return characterService.findAllCharactersInMovie(id);
    }
}
