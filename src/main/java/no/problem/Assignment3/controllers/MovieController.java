package no.problem.Assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * MovieController that is used as an itermediary between the user and the
 * Services. The MovieController uses the method calls from the MovieService
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/movie") // da base
public class MovieController {
    @Autowired
    MovieService movieService;

    @Operation(summary = "Gets all Movies")
    @GetMapping("/all")
    public ResponseEntity<List<Movie>> getAllMovies() {
        return movieService.getAllMovies();
    }

    @Operation(summary = "Gets a specific Movie using its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie has been found"),
            @ApiResponse(responseCode = "400", description = "Movie could not be found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getSpecificMovie(@PathVariable Long id) {
        return movieService.getSpecificMovie(id);
    }

    @Operation(summary = "Adds a new Movie to the database")
    @ApiResponse(responseCode = "201", description = "Movie has been added")
    @PostMapping("/add")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie m) {
        return movieService.addMovie(m);
    }

    @Operation(summary = "Updates a Movie using its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Movie has been updated"),
            @ApiResponse(responseCode = "400", description = "Movie for update could not be found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie m, @PathVariable Long id) {
        return movieService.updateMovie(m, id);
    }

    @Operation(summary = "Deletes a Movie using its Id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Set<Movie>> deleteMovie(@PathVariable Long id) {
        return movieService.deleteMovie(id);
    }

    @Operation(summary = "Updates the Characters casting in a Movie using a List<Long> (ids) and MovieId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Characters in movie have been updated"),
            @ApiResponse(responseCode = "400", description = "Movie for update could not be found")
    })
    @PostMapping("/update/{id}")
    public ResponseEntity<Set<MovieCharacter>> updateMovieCharacters(@RequestBody List<Long> newSet, @PathVariable Long id) {
        return movieService.updateMovieCharacters(newSet, id);
    }
}

