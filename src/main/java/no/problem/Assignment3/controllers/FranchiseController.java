package no.problem.Assignment3.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.problem.Assignment3.models.Franchise;
import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.services.CharacterService;
import no.problem.Assignment3.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * FranchiseController that is used as an itermediary between the user and the
 * Services. The FranchiseController uses the method calls from the FranchiseService
 */
@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/franchise") // da base
public class FranchiseController {
    @Autowired
    CharacterService characterService;
    @Autowired
    FranchiseService franchiseService;

    @Operation(summary = "Gets all Franchises")
    @GetMapping("/all")
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }

    @Operation(summary = "Gets a single Franchise using its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise has been found"),
            @ApiResponse(responseCode = "400", description = "Franchise could not be found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getSpecificFranchise(@PathVariable Long id) {
        return franchiseService.getSpecificFranchise(id);
    }

    @Operation(summary = "Adds a single Franchise to the database")
    @ApiResponse(responseCode = "201", description = "Franchise has been added")
    @PostMapping("/add")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise f) {
        return franchiseService.addFranchise(f);
    }

    @Operation(summary = "Updates a single Franchise using its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Franchise has been updated"),
            @ApiResponse(responseCode = "400", description = "Franchise for update could not be found")
    })
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@RequestBody Franchise f, @PathVariable Long id) {
        return franchiseService.updateFranchise(f,id);
    }

    @Operation(summary = "Updates a Franchises Movies using the FranchisesId and a List<Long> of MovieIds")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Movies in franchise have been updated"),
            @ApiResponse(responseCode = "400", description = "Franchise for update could not be found")
    })
    @PostMapping("/update/{id}")
    public ResponseEntity<Set<Franchise>> updateMovies(@RequestBody List<Long> newSet, @PathVariable Long id) {
        return franchiseService.updateMovies(newSet, id);
    }

    @Operation(summary = "Deletes a single Franchise using its Id")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Set<Franchise>> deleteFranchise(@PathVariable Long id) {
        return franchiseService.deleteFranchise(id);
    }

    @Operation(summary = "Gets all movies in a Franchise using the Franchises Id")
    @GetMapping("/movies/{id}")
    public ResponseEntity<Set<Movie>> findAllMoviesInFranchise(@PathVariable Long id) {
        return franchiseService.findAllMoviesInFranchise(id);
    }

    @Operation(summary = "Gets all MovieCharacters in a Franchise using the Franchises id")
    @GetMapping("/characters/{id}")
    public ResponseEntity<Set<MovieCharacter>> findAllCharactersInFranchise(@PathVariable Long id) {
        return franchiseService.findAllCharactersInFranchise(id);
    }
}

