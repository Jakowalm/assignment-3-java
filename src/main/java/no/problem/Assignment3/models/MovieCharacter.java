package no.problem.Assignment3.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * MovieCharacter PostgreSQL-Schema
 * Contains columns and their types, getters, setters and
 * the relationship this table has with the Movie-table
 */
@Entity
@Table(name="Character")
public class MovieCharacter {
    // Auto-generate ID for new elements. Incremented by 1
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Columns in the MovieCharacter-schema
    @Column(name = "full_name")
    private String fullName; // "Dwayne Johnson"

    @Column(name = "alias")
    private String alias; // "The Rock"

    @Column(name = "gender")
    private String gender; // male/female

    @Column(name = "picture")
    private String picture; // url

    // Several MovieCharacters can be in several Movies
    @ManyToMany
    @JoinTable(
            name = "movie_id",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )

    /*
       Use JsonGetter so that when we do queries on MovieCharacters,
       the Movies that the character is casting in, is simply shown by
       their MovieTitle
     */
    private Set<Movie> movies;
    @JsonGetter("movies")
    public Set<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(Movie::getMovieTitle).collect(Collectors.toSet());
        }
        return null;
    }

    // Generated getters and setters below //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieCharacter that = (MovieCharacter) o;
        return id.equals(that.id) && fullName.equals(that.fullName) && Objects.equals(alias, that.alias) && gender.equals(that.gender) && picture.equals(that.picture) && movies.equals(that.movies);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
