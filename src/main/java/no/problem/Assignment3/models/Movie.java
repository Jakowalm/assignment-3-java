package no.problem.Assignment3.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Movie PostgreSQL-Schema
 * Contains columns and their types, getters, setters and
 * the relationship this table has with the MovieCharacter and Franchise- tables
 */
@Entity
@Table(name="Movie")
public class Movie {
    // Auto-generate ID for new elements. Incremented by 1
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Columns in the Movie-schema
    @Column(name = "movie_title")
    private String movieTitle; // "Titanic"

    @Column(name = "genre")
    private String genre; // "Comedy"

    @Column(name = "release_year")
    private String releaseYear; // "1997"

    @Column(name = "director")
    private String director; // Steven Spielberg

    @Column(name = "picture")
    private String picture; // url

    @Column(name = "trailer")
    private String trailer; // url

    // Several movies can be in a single franchise
    @ManyToOne
    @JoinColumn(name = "franchise")
    private Franchise franchise;

    // Several movies can contain several characters
    @ManyToMany(mappedBy = "movies")
    private Set<MovieCharacter> movieCharacters;

    // Generated getters and setters below //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id && movieTitle.equals(movie.movieTitle) && genre.equals(movie.genre) && releaseYear.equals(movie.releaseYear) && director.equals(movie.director) && picture.equals(movie.picture) && trailer.equals(movie.trailer) && Objects.equals(franchise, movie.franchise) && movieCharacters.equals(movie.movieCharacters);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Set<MovieCharacter> getCharacters() {
        return movieCharacters;
    }

    public void setCharacters(Set<MovieCharacter> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }
}
