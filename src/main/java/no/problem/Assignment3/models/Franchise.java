package no.problem.Assignment3.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Franchise PostgreSQL-Schema
 * Contains columns and their types, getters, setters and
 * the relationship this table has with the Movie-table
 */
@Entity
@Table(name="Franchise")
public class Franchise {
    // Auto-generate ID for new elements. Incremented by 1
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Columns in the Franchise-schema
    @Column(name = "name")
    private String name; // "Marvel Universe"

    @Column(name = "description")
    private String description; // "A universe brimming with superheroes"

    // A single franchise can have many movies
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

    /*
       Use JsonGetter so that when we do queries on a Franchise,
       the Movies belonging to that Franchise are only shown
       by their MovieTitle
     */
    @JsonGetter("movies")
    public Set<String> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(Movie::getMovieTitle).collect(Collectors.toSet());
        }
        return null;
    }

    // Generated getters and setters below //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Franchise franchise = (Franchise) o;
        return id.equals(franchise.id) && name.equals(franchise.name) && description.equals(franchise.description) && movies.equals(franchise.movies);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
