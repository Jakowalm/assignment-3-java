package no.problem.Assignment3.repositories;

import no.problem.Assignment3.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CharacterRepository extends JpaRepository<MovieCharacter, Long> {
    Set<MovieCharacter> findMovieCharactersByMoviesId(Long id);
}
