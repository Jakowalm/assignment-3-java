package no.problem.Assignment3.components;

import no.problem.Assignment3.controllers.CharacterController;
import no.problem.Assignment3.controllers.FranchiseController;
import no.problem.Assignment3.controllers.MovieController;
import no.problem.Assignment3.models.Franchise;
import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.repositories.CharacterRepository;
import no.problem.Assignment3.repositories.FranchiseRepository;
import no.problem.Assignment3.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Runs on startup, seeds the characters, movies and franchises with some
 * predefined-startup values
 */
@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    CharacterController characterController;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    FranchiseController franchiseController;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Seeding --- Puts 3 Characters, 3 Movies, 2 Franchises into the DB on startup.
        Franchise franchise1 = new Franchise(); // marvel
        Franchise franchise2 = new Franchise(); // lotr
        Movie movie1 = new Movie();
        Movie movie2 = new Movie();
        Movie movie3 = new Movie();
        MovieCharacter bale = new MovieCharacter();
        MovieCharacter hardy = new MovieCharacter();
        MovieCharacter samuel = new MovieCharacter();
        franchiseRepository.save(franchise1);
        franchiseRepository.save(franchise2);

        movie1.setId(1L);
        movie1.setMovieTitle("Warrior");
        movie1.setGenre("amazing");
        movie1.setReleaseYear("2008");
        movie1.setDirector("Bruce");
        movie1.setPicture("https://ftw.usatoday.com/wp-content/uploads/sites/90/2017/05/spongebob.jpg");
        movie1.setTrailer("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
        movie1.setFranchise(franchise1);
        movieRepository.save(movie1);

        movie2.setId(2L);
        movie2.setMovieTitle("Whiplash");
        movie2.setGenre("boring");
        movie2.setReleaseYear("6969");
        movie2.setDirector("Speilborg");
        movie2.setPicture("https://ftw.usatoday.com/wp-content/uploads/sites/90/2017/05/spongebob.jpg");
        movie2.setTrailer("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
        movie2.setFranchise(franchise1);
        movieRepository.save(movie2);

        movie3.setId(3L);
        movie3.setMovieTitle("Sneks'n plens");
        movie3.setGenre("wowza");
        movie3.setReleaseYear("4200");
        movie3.setDirector("Hitchkock");
        movie3.setPicture("https://ftw.usatoday.com/wp-content/uploads/sites/90/2017/05/spongebob.jpg");
        movie3.setTrailer("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
        movie3.setFranchise(franchise2);
        movieRepository.save(movie3);

        bale.setId(1L);
        bale.setFullName("Bruce Wayne");
        bale.setAlias("Bwayne");
        bale.setGender("male");
        bale.setPicture("https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Set<Movie> balesMovies = new HashSet<Movie>();
        balesMovies.add(movie1);
        bale.setMovies(balesMovies);
        characterController.addMovieCharacter(bale);
        characterRepository.save(bale);

        hardy.setId(2L);
        hardy.setFullName("Tom Hardy");
        hardy.setAlias("Hardly");
        hardy.setGender("male");
        hardy.setPicture("https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Set<Movie> hardysMovies = new HashSet<Movie>();
        hardysMovies.add(movie1);
        hardysMovies.add(movie2);
        hardy.setMovies(hardysMovies);
        characterController.addMovieCharacter(hardy);
        characterRepository.save(hardy);

        samuel.setId(3L);
        samuel.setFullName("Samuel Jackson");
        samuel.setAlias("Ell");
        samuel.setGender("male");
        samuel.setPicture("https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Set<Movie> samuelMovies = new HashSet<Movie>();
        samuelMovies.add(movie3);
        samuel.setMovies(samuelMovies);
        characterController.addMovieCharacter(samuel);
        characterRepository.save(samuel);

        franchise1.setId(1L);
        franchise1.setName("Marvel");
        franchise1.setDescription("The frenchest franchise");
        Set<Movie> franchise1Movies = new HashSet<Movie>();
        franchise1Movies.add(movie1);
        franchise1Movies.add(movie2);
        franchise1.setMovies(franchise1Movies);
        franchiseController.addFranchise(franchise1);
        franchiseRepository.save(franchise1);

        franchise2.setId(2L);
        franchise2.setName("Lord of the Rings");
        franchise2.setDescription("The dankest of squads");
        Set<Movie> franchise2Movies = new HashSet<Movie>();
        franchise2Movies.add(movie3);
        franchise2.setMovies(franchise2Movies);
        franchiseController.addFranchise(franchise2);
        franchiseRepository.save(franchise2);
    }
}