package no.problem.Assignment3.services;

import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.repositories.CharacterRepository;
import no.problem.Assignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    CharacterRepository characterRepository;

    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> data = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    public ResponseEntity<Movie> getSpecificMovie(Long id){
        HttpStatus status;
        Movie movie = new Movie();
        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movie, status);
        }
        movie = movieRepository.getById(id);
        status = HttpStatus.OK;
        return new ResponseEntity<>(movie, status);
    }

    public ResponseEntity<Movie> addMovie(Movie m){
        Movie movie = movieRepository.save(m);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    public ResponseEntity<Movie> updateMovie(Movie m, Long id) {
        HttpStatus status;
        Movie movie = new Movie();
        if(!Objects.equals(id, m.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(movie, status);
        }
        movie = movieRepository.save(m);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(movie, status);
    }

    public ResponseEntity<Set<Movie>> deleteMovie(Long id){
        HttpStatus status = HttpStatus.OK;
        characterRepository.deleteById(id);
        return new ResponseEntity<>(new HashSet<>(),status);
    }

    public ResponseEntity<Set<MovieCharacter>> updateMovieCharacters(List<Long> newSet, Long id) {
        Movie movie = movieRepository.getById(id);
        HttpStatus status = HttpStatus.OK;
        Set<MovieCharacter> characterSet = movie.getCharacters();
        for (Long charId: newSet) {
            MovieCharacter newCharacter = characterRepository.getById(charId);
            characterSet.add(newCharacter);
        }
        movie.setCharacters(characterSet);
        return new ResponseEntity<>(movie.getCharacters(),status);
    }
}
