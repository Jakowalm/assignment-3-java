package no.problem.Assignment3.services;

import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.repositories.CharacterRepository;
import no.problem.Assignment3.repositories.FranchiseRepository;
import no.problem.Assignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class CharacterService {

    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    MovieRepository movieRepository;

    public ResponseEntity<Set<MovieCharacter>> findAllCharactersInMovie(Long id) {
        HttpStatus status = HttpStatus.OK;
        Movie movie = movieRepository.getById(id);
        Set<MovieCharacter> characters = characterRepository.findMovieCharactersByMoviesId(id);//movie);
        return new ResponseEntity<>(movie.getCharacters(), status);
    }

    public ResponseEntity<Set<MovieCharacter>> deleteMovieCharacter(Long id){
        HttpStatus status = HttpStatus.OK;
        characterRepository.deleteById(id);
        return new ResponseEntity<>(new HashSet<>(),status);
    }

    public ResponseEntity<MovieCharacter> updateMovieCharacter(MovieCharacter mc, Long id){
        HttpStatus status;
        MovieCharacter movieChar = new MovieCharacter();
        if(!Objects.equals(id, mc.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(movieChar, status);
        }
        movieChar = characterRepository.save(mc);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(movieChar, status);
    }

    public ResponseEntity<MovieCharacter> addMovieCharacter(MovieCharacter mc){
        MovieCharacter movieChar = characterRepository.save(mc);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movieChar, status);
    }

    public ResponseEntity<MovieCharacter> getSpecificMovieCharacter(Long id){
        HttpStatus status;
        MovieCharacter movieChar = new MovieCharacter();
        if(!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movieChar, status);
        }
        movieChar = characterRepository.getById(id);
        status = HttpStatus.OK;
        return new ResponseEntity<>(movieChar, status);
    }

    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> data = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

}
