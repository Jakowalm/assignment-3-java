package no.problem.Assignment3.services;

import no.problem.Assignment3.models.Franchise;
import no.problem.Assignment3.models.Movie;
import no.problem.Assignment3.models.MovieCharacter;
import no.problem.Assignment3.repositories.FranchiseRepository;
import no.problem.Assignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class FranchiseService {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;

    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> data = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    public ResponseEntity<Franchise> getSpecificFranchise(Long id){
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(franchise, status);
        }
        franchise = franchiseRepository.getById(id);
        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Franchise> addFranchise(Franchise f){
        Franchise franchise = franchiseRepository.save(f);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Franchise> updateFranchise(Franchise f, Long id){
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!Objects.equals(id, franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(franchise, status);
        }
        franchise = franchiseRepository.save(f);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Set<Franchise>> updateMovies(List<Long> newSet, Long id) {
        Franchise franchise = franchiseRepository.getById(id);
        Set<Movie> movieSet = franchise.getMovies();
        HttpStatus status = HttpStatus.OK;
        for (Long charId: newSet) {
            Movie newMovie = movieRepository.getById(charId);
            movieSet.add(newMovie);
        }
        franchise.setMovies(movieSet);
        return new ResponseEntity<>(new HashSet<>(), status);
    }

    public ResponseEntity<Set<Franchise>> deleteFranchise(@PathVariable Long id){
        HttpStatus status = HttpStatus.OK;
        franchiseRepository.deleteById(id);
        return new ResponseEntity<>(new HashSet<>(),status);
    }

    public ResponseEntity<Set<Movie>> findAllMoviesInFranchise(Long id) {
        HttpStatus status = HttpStatus.OK;
        Franchise franchise = franchiseRepository.getById(id);
        return new ResponseEntity<>(franchise.getMovies(), status);
    }

    public ResponseEntity<Set<MovieCharacter>> findAllCharactersInFranchise(Long id) {
        HttpStatus status = HttpStatus.OK;
        Franchise franchise = franchiseRepository.getById(id);
        HashSet<MovieCharacter> characters = new HashSet<>();
        for (Movie movie: franchise.getMovies()) {
            characters.addAll(movie.getCharacters());
        }
        return new ResponseEntity<>(characters, status);
    }
}
