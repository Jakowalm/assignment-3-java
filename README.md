# ~~IMDB~~ IMNP - Internet Movies No Problem

Welcome to the "no problem" movie database! The project uses
[SQLite](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc)
to access the
[PostgreSQL](https://www.postgresql.org/)
database.

Springdocs documentation with Swagger can be found on [localhost:8080/swagger-documentation.html](localhost:8080/swagger-documentation.html)
when the program is running.

## Database

The has three tables:

For more information on the requests, look at the Swagger documentation.


### Character

This table has information about movie characters and what movies they appear in.

It has the following endpoints under /api/character:
- /all - (GET)
- /{id} - (POST and GET)
- /add - (POST)
- /delete/{id} - (DELETE)
- /movie/{id} - (GET)

### Movie

This table has information about movies, their characters and what franchises they might be part of.

It has the following endpoints under /api/movie:
- /all - (GET)
- /{id} - (POST and GET)
- /add - (POST)
- /delete/{id} - (DELETE)
- /update/{id} - (POST)

### Franchise

This table has information about franchises and what movies they contain.

It has the following endpoints under /api/franchise:
- /all - (GET)
- /{id} - (POST and GET)
- /add - (POST)
- /delete/{id} - (DELETE)
- /update/{id} - (POST)
- /movies/{id} - (GET)
- /characters/{id} - (GET)